<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Hệ thống bán hàng đa kênh</title>
    <link rel="stylesheet" href="{{ asset('Ecomerce/plugins/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('Ecomerce/plugins/font-awesome/css/font-awesome.min.css') }}">
</head>
<body style="margin: 0;padding: 0;">
<div id="app">
    <Ecomerce></Ecomerce>
</div>
<script>
    window.Laravel = {
        'csrfToken': '{{ csrf_token() }}'
    }
</script>
<script src="{{ asset('Ecomerce/plugins/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
