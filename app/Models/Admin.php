<?php
//
//namespace App\Models;
//
//use Illuminate\Contracts\Auth\MustVerifyEmail;
//use Illuminate\Foundation\Auth\Admin as Authenticatable;
//use Illuminate\Notifications\Notifiable;
//
//class Admin extends Authenticatable
//{
//    use Notifiable;
//
//    /**
//     * The attributes that are mass assignable.
//     *
//     * @var array
//     */
//    protected $fillable = [
//        'admin_avatar',
//        'admin_fullname',
//        'admin_birthday',
//        'admin_phone',
//        'admin_address',
//        'district_id',
//        'city_id',
//        'admin_role_id',
//        'admin_name',
//        'admin_email',
//        'admin_password',
//        'admin_status'
//    ];
//
//    /**
//     * The attributes that should be hidden for arrays.
//     *
//     * @var array
//     */
//    protected $hidden = [
//        'admin_password', 'remember_token',
//    ];
//
//    /**
//     * The attributes that should be cast to native types.
//     *
//     * @var array
//     */
//    protected $casts = [
//        'admin_email_verified_at' => 'datetime',
//    ];
//}
