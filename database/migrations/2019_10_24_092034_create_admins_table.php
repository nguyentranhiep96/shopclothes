<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('admin_avatar')->nullable();
            $table->string('admin_fullname')->nullable();
            $table->string('admin_birthday');
            $table->string('admin_phone')->nullable();
            $table->integer('admin_address')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('admin_role_id');
            $table->string('admin_username');
            $table->string('admin_email')->unique();
            $table->timestamp('admin_email_verified_at')->nullable();
            $table->string('admin_password');
            $table->integer('admin_status')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
